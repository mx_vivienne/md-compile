def join_varargs(separator, *args):
    list = []
    for arg in args:
        list.append(str(arg))
    return separator.join(list)
