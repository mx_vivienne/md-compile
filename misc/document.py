class Document:
    def __init__(self, blocks, depth=False):
        self.blocks = blocks
        self.depth = depth
        self.variables = {
            "dest_file": ""
        }

    def print(self):
        print("[Document object]")
        print("  Variables:")
        for k,v in self.variables.items():
            print("    " + str(k) + ": " + str(v))
        print("  Blocks:")
        for b in self.blocks:
            print("    block:")
            print("      content  : " + str(b.content))
            print("      type     : " + str(b.type))
            print("      condition: " + str(b.condition))
            print("      depth    : " + str(b.depth))
