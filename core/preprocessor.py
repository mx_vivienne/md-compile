import re
from misc.document import *
from misc.helpers import *

def empty(*args):
    """Return an empty string."""
    return ""

class TextBlock:
    """Represents a part of the document up until the next if-statement.

    Type and condition refer to the statement thats *closing* the current block!
    content -- the text content of this block, not including the if-statement
    type -- "global", "if", "elseif", "else" or "endif"
    depth -- indicates the number of nested if-statements the block is in
    condition -- the condition of the statement, if any
    """

    def __init__(self, content, type, depth, condition=None):
        """Return a new TextBlock object"""
        self.content = content
        # possible types: if, else, elseif, endif, global
        self.type = type
        self.depth = depth
        # list of parameters
        self.condition = condition

class Preprocessor:
    """Process directives inside double curly braces.

    Preprocess the given document by evaluating all recognized directives,
    and return a number of outputs ready to be transpiled to HTML.

    class variables:
    _var_table -- contains all variables accessible by directives

    compiled regex patterns:
    _re_dir -- all pairs of {{ }} brackets with arbitrary content inbetween
    _re_comment -- comments of the form {{! !}}
    _re_include -- {{include}} directive with optional parameters after a whitespace
    _re_ifstat -- any if statement, with optional parameters
    _re_buildall -- {{buildall}} with optional parameters after whitespace
    """

    _re_dir      = re.compile(r"\{\{.*?\}\}", re.DOTALL)
    _re_comment  = re.compile(r"\{\{!.*?!\}\}", re.DOTALL)
    _re_include  = re.compile(r"\{\{include( .*?)?\}\}", re.DOTALL)
    _re_ifstat   = re.compile(r"\{\{(if|elseif|else|endif)( .*?)?\}\}", re.DOTALL)
    _re_buildall = re.compile(r"\{\{build_all( .*?)?\}\}", re.DOTALL)

    # def _init_vars():
    #     """Initiate variables for use by directives."""
    #     Preprocessor._var_table = {
    #         "base_url": "https://enby.services",
    #         "root_dir": "var/www/html",
    #         "dest_file": ""}

    def prepare(input):
        """Preprocess input document, and return the result(s).

        Process all includes first, remove comments, then structure into blocks,
        breaking the document up at all if-statements (also else, elseif, and so on).
        Lastly, find the first build_all in a global block (not inside any if),
        process it and delete all others.
        """
        # first do {{include}}
        inc = Preprocessor._re_include
        while True:
            match = inc.search(input)

            if (None is match):
                break

            # read directive, get file content and do replacement
            # TODO probably some warnings?
            match = _remove_braces(match.group(0)).split()
            repl = ""
            with open(match[1]) as f:
                repl = f.read()
            input = re.sub(inc, repl, input, count=1)

        # remove all comments next
        input = re.sub(Preprocessor._re_comment, "", input)

        # structure document, breaking it up at else/if or endif statements
        blocks = []
        breakloop = False
        depth = 0

        temp = TextBlock(input, "global", 0)

        # how this loop works:
        # (the unprocessed rest of the document is always in `temp.content`)
        # during one iteration of the loop:
        # + temp is new block, gets appended
        # + correct content of new block
        # + create new TextBlock (into temp) with rest of the document,
        #   and with the other vars according to the following statement
        # basically temp is always the newest block, but its content
        # gets corrected on the next iteration when the next statement is found
        while (not breakloop):
            blocks.append(temp)
            ifstat = Preprocessor._re_ifstat

            rest = blocks[len(blocks)-1].content
            match = ifstat.search(rest)

            if (None is match):
                breakloop = True
            else:
                params = _remove_braces(match.group(0)).split()

                # correct content of prev block, only stuff up to the directive
                blocks[len(blocks)-1].content = rest[:match.start()]
                # rest of the document is set as content for current block
                content = rest[match.end():]
                type = params[0]

                condition=None
                if (type == "if") or (type == "elseif"):
                    condition = params[1:4]

                    if (type == "if"):
                        depth += 1

                elif (type == "endif"):
                    depth = max(0, depth-1)

                if (depth == 0):
                    type = "global"

                # TODO make sure the type matches up with the parameters

                # this block is added to the list on the next loop iteration
                temp = TextBlock(content, type, depth, condition)

        # [0] is the varible name, all others are the values to use for it
        build_values = []

        # search for the first {{build_all}} in a global block
        for b in blocks:
            if ("global" == b.type):
                match = Preprocessor._re_buildall.search(b.content)
                if match:
                    build_values = _remove_braces(match.group(0)).split()[1:]
                    break

        # delete all other {{build_all}}
        for b in blocks:
            b.content = re.sub(Preprocessor._re_buildall, "", b.content)

        prepared = []

        # start build processes
        if (2 <= len(build_values)):
            for val in build_values[1:]:
                doc = Document(blocks)
                doc.variables[build_values[0]] = val
                prepared.append(_build(blocks, doc))
        else:
            doc = Document(blocks)
            prepared.append(_build(blocks, doc))

        return prepared

    def _dir_get(doc, var, *junk):
        """Look up var and return its value, or "" if unknown."""
        table = doc.variables
        if (var in table):
            return table[var]
        return ""

    def _dir_set(doc, var, *value):
        """Set var to the given value."""
        doc.variables[var] = join_varargs(" ", *value)
        return ""

    def _dir_unset(doc, var, *junk):
        """Delete var from the table"""
        table = doc.variables
        if (var in table):
            del table[var]
        return ""

    # TODO add other directives

# these are not part of the Preprocessor class anymore!

def _build(blocks, document):
    """Take a prepared input document and evaluate blocks and directives."""

    if (0 == len(blocks)):
        return ""

    b = blocks[0]
    # print(b.type)
    if (0 == b.depth):
        return _build_block(b, document) + _build(blocks[1:], document)

    # block with any level of nesting
    else:
        depth = b.depth
        bool = True
        if (b.type in ["if", "elseif"]):
            bool = _eval(b.condition, document.variables)

        # evaluated to true
        if (bool):
            # process current block
            output = _build_block(b, document)
            i = start = 1

            # process nested blocks, and following endif blocks on the same depth
            while True:
                while (i < len(blocks) and depth < blocks[i].depth):
                    i += 1
                output += _build(blocks[start:i], document)

                if (i < len(blocks) and "endif" == blocks[i].type):
                    output += _build_block(blocks[i],document)
                    i += 1
                    start = i
                else:
                    break

            # skip all following blocks of >= depth, and process the rest of the document
            while (i < len(blocks) and depth <= blocks[i].depth):
                i += 1
            rest = blocks[i:]

            return output + _build(rest, document)

        # evaluated to false
        else:
            i = 1
            while True:
                while (i < len(blocks) and depth < blocks[i].depth):
                    i += 1
                if (i >= len(blocks) or (blocks[i].type != "endif")):
                    break

            return _build(blocks[i:], document)


def _build_block(block, document):
    """Process all directives in a single block."""
    dir = Preprocessor._re_dir
    content = block.content

    while True:
        match = dir.search(content)
        if (None is match):
            break

        # remove braces and split into parameters
        match = _remove_braces(match.group(0)).split()

        #find command, evaluate it, and replace the directive
        func = getattr(Preprocessor, "_dir_" + match[0], empty)
        repl = func(document, *match[1:])
        content = re.sub(dir, repl, content, count=1)
    return content


def _handle_if(block, document):
    if (block.type == "global"):
        return True

    if (block.type == "endif"):
        if (document.depth):
            if (block.depth >= document.depth):
                return False
            else:
                document.depth = False
        return True

    # skip if an earlier if a lower conditional evaluated to false,
    # or one of equal depth has been processed (ie. skip the else after an if)

    # if (False !=document.depth):
    #     return True
    # if (block.depth >= document.depth):
    #     return False

    if (block.type in ["if","elseif"]):
        bool = _eval(block.condition, document.variables)
        if (bool):
            document.depth = block.depth
        return bool
    else: #block.type == "else"
        document.depth = block.depth
        return True

def _eval(expr, vars):
    c = expr[0]
    if (c == "def"):
        return (expr[1] in vars)
    if (c == "ndef"):
        return (not expr[1] in vars)

    x1,x2 = _get_param(expr[1], vars), _get_param(expr[2], vars)

    if (c == "eq"):
        return (x1 == x2)
    if (c == "neq"):
        return (x1 != x2)

    x1,x2 = int(x1),int(x2)

    if (c == "gt"):
        return (x1 > x2)
    if (c == "geq"):
        return (x1 >= x2)
    if (c == "lt"):
        return (x1 < x2)
    if (c == "leq"):
        return (x1 <= x2)

    return false

def _get_param(x, vars):
    if ("$" == x[0]):
        if (x[1:] in vars):
            return vars[x[1:]]
        else:
            return ""
    else:
        return x

def _remove_braces(str):
    """Return str without enclosing {{}}"""
    return str[2:-2]
