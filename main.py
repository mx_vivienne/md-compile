from core.preprocessor import Preprocessor

s = "{{build_all lang en de fr}}{{if eq $lang en}}Hello world!{{elseif eq $lang de}}Hallo Welt!{{endif}} <3"

outputs = Preprocessor.prepare(s)

print(str(outputs))
