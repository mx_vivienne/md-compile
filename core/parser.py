import re
from misc.regex import *
from misc.elements import Element as E

class Node:
    def __init__(self, name, open, content=[], children=[], is_container=False):
        self.name = name
        self.open = open
        # content is a list of strings each representing one line of text
        self.content = content
        self.children = children
        # node can only have other nodes as children when its a container
        self.is_container = is_container

def parse(markdown):
    return _parse_inlines(_parse_blocks(markdown))

def _parse_blocks(markdown):
    """Parse block structure of the document and construct tree representation."""

    # root node of the document tree
    tree = Node("document", True, "", [])

    for line in re.split(re_line_break, markdown):
        # keeping track of position of current substr so tabs can be expanded correctly
        linepos = 0

        #get all open nodes
        #open = [document]
        #while (0 < len(open[-1].children) and open[-1].children[-1].open):
        #    new_node = open[-1].children[-1]

        while (True):
            ch = open[-1].children
            if (0 < len(ch) and ch[-1].open):
                new_node = ch[-1]
            else:
                break

        line = _expand_leading_tabs(line, linepos)
        rest, restpos = _match_nodes(open, line, linepos)
        if (open[-1].name == E.HTML_BLOCK and not open[-1].open):
            open[-1].content.append(line)
            _close_unmatched(open)
        else:
            new_block, num = _block_starts(tree, rest, restpos, line)
            if (new_block):
                _close_unmatched(open)
                #create a new node and add to last open matched node

        #TODO

    return tree

def _parse_inlines(tree):
    return "TO BE IMPLEMENTED"

def _block_starts(tree, rest, restpos, line):
    rest = _expand_leading_tabs(rest, restpos)

    if re_thematic_break.match(rest):
        return E.THEMATIC_BREAK

    if re_atx_heading.match(rest):
        return E.ATX_HEADING

    # TODO make sure list elements have precedence over setext headings
    if re_setext_underline.match(rest):
        return E.SETEXT_HEADING

    if re_code_block_marker.match(rest):
        return E.CODE_INDENT

    if re_fence_marker_open.match(rest):
        return E.CODE_FENCED

    for i in range(7):
        html_re = re_html_block_start[i]
        if html_re.match(rest):
            return E.HTML_BLOCK, i

    #TODO link reference definitions

    return False

def _match_nodes(nodes, line, linepos):
    for n in nodes:
        n.matched = False
        name = node.name

        wspace = _get_whitespace(line)
        stripped = line[wspace:]

        if (E.DOCUMENT == name):
            n.matched = True

        elif (E.CODE_INDENT == name):
            if (3 < len(wspace)):
                n.matched = True
                line = line[4:]
                linepos += 4

        elif (E.CODE_FENCED == name):
            match = re_fence_marker_close.match(stripped)
            if (match and (match.group(1)[0] == n.char) and len(match.group(1)) >= n.length):
                n.open = False
            else:
                n.matched = True
                x = min(wspace, n.indentation)
                line = line[x:]
                linepos += x

        elif (E.HTML_BLOCK == name):
            end_re = re_html_block_end[n.type]
            match = end_re.match(line)
            n.matched = True
            if (match):
                n.open = False
                break

        elif (name in [E.PARAGRAPH, E.LINK_DEFINITION]):
            if (not re_blank_line.search(line)):
                n.matched = True

        elif (E.BLOCK_QUOTE == name):
            match = re_block_quote_marker.match(line)
            if (match):
                n.matched = True
                line = line[match.end():]
                linepos += match.end()

        elif (E.LIST == name):
            match = re_ul_marker.match(list) or re_ol_marker.match(list)
            if (match and match.group(1) == node.char):
                n.matched = True
                line = line[match_ul.end():]
                linepos += match_ul.end()
                break
    return line, linepos

def _get_whitespace(line):
    match = re_not_whitespace.search(line)
    if (match):
        return match.start()
    return len(line)


def _expand_leading_tabs(substr, linepos):
    """expands all tabs in leading whitespace"""

    expanded = ""

    while (0 < len(substr)):
        match = re_whitespace.match(substr)

        if ("\t" == substr[0]):
            amount = 4 - (linepos % 4)
            expanded += " " * amount
            linepos += amount
            substr = substr[1:]

        elif (match):
            expanded += match.group(0)
            substr = substr[match.end():]
            linepos += 1

        else:
            break

    return expanded + substr


def _validate_link_reference(string):
    match = re_link_reference.match(string)
    if (match):
        label, dest, title = match.group(1), match.group(2), match.group(3)

        # validate label
        if (999 < len(label) or not re_not_whitespace.search(label)):
            return false

        #validate destination
        if ("<" != dest[0]):
            if (re_ascii_control.search(dest)):
                return false

            #validate parentheses
            if (not do_parens_match(dest)):
                return false

        # normalize label
        label = label.lower()
        # remove delimiter characters
        if (title):
            title = title[1:-1]

        return [label, dest, title]
    return false

def do_parens_match(input):
    count = 0
    while True:
        paren = re_paren_chars.search(input)

        if (not paren):
            return (0 == count)

        input = input[paren.end():]

        if ("(" == paren.group(0)):
            count += 1
        else:
            count -= 1

        if (0 > count):
            return false
