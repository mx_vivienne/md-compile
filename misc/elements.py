from enum import Enum, auto

class Element(Enum):
    DOCUMENT        = "document"
    THEMATIC_BREAK  = "thematic_break"
    ATX_HEADING     = "atx_heading"
    SETEXT_HEADING  = "setext_heading"
    CODE_INDENT     = "code_indent"
    CODE_FENCED     = "code_fenced"
    HTML_BLOCK      = "html_block"
    LINK_DEFINITION = "link_definition"
    PARAGRAPH       = "paragraph"
    BLOCK_QUOTE     = "block_quote"
    LIST            = "list"
