import re

_html_6_tags = "|".join([
    "address", "article", "aside",
    "base(?:font)?", "blockquote", "body",
    "caption", "center", "col(?:group)?",
    "d[dlt]", "details", "dialog", "di[rv]",
    "fieldset", "figcaption", "figure", "footer", "form", "frame(?:set)?",
    "h[1-6r]", "head(?:er)?", "html",
    "iframe",
    "legend", "li(?:nk)?",
    "main", "menu(?:item)?",
    "nav", "noframes",
    "[ou]l", "optgroup", "option",
    "p(?:aram)?",
    "section", "source", "summary",
    "table", "tbody", "t[dhr]", "tfoot", "thead", "title", "track"
])
_html_value = r"[^ \t\n\v\f\r\"'=<>`]{1,}|['\"].*?['\"]"
_html_attribute = r" +[_:a-zA-Z][-_.:a-zA-Z0-9]*( *= *" + _html_value + r")?"
_html_tag_name = r"[a-zA-Z][-.a-zA-Z]*"
_html_open_tag = r"<(" + _html_tag_name + r")(" + _html_attribute + r")* */?>"
_html_close_tag = r"</(" + _html_tag_name + r") *>"

re_thematic_break = re.compile(r"^ {:3}(?:(?:\* *){3,})|(?:(?:_ *){3,})|(?:(?:- *){3,})$")
# group(1) is first string of #, group(2) is inline content
re_atx_heading = re.compile(r"^ {:3}(#{1,6}) +(.*?)(?: +#*)? *$")
# group(1) is underline
re_setext_underline = re.compile(r"^ {:3}(-+|=+) *$")
re_blank_line = re.compile(r"^[ \t]*$")
# group(1) is content of link label (not validated or normalized yet!)
# group(2) is link destination (not validated either!)
# group(3) is the optional title, including delimiter characters
re_link_reference = re.compile(r"""\ {,3}\[([^\[\]]*?[^\\])\]:
                                   \s*
                                   (<[^<>\n]*?>|[^<\s]\S*)
                                   \s*
                                   (?:\s(".*?"|'.*?'|\(.*?\))?$""",
                                   re.VERBOSE | re.DOTALL | re.ASCII)

# group(1) is bullet character
re_ul_marker = re.compile(r" {,3}([*-+]) ")
# group(1) is delimimter character
re_ol_marker = re.compile(r" {,3}[0-9]{1,10}([.)]) ")
# group(1) is fence, group(2) is info string
re_fence_marker_open = re.compile(r"(`{3,}|~{3,}) *(.*?) *$")
# group(1) is fence
re_fence_marker_close = re.compile(r"(`{3,}|~{3,}) *$")
re_block_quote_marker = re.compile(r" {,3}> ?")
re_code_block_marker = re.compile(r" {4}")
re_link_reference_marker = re.compile(r" {,3}\[")

re_html_block_start = [
    re.compile(r"^(<script|<pre|<style)(?:>| |$)", re.IGNORECASE),
    re.compile(r"^<!--"),
    re.compile(r"^<?"),
    re.compile(r"^<![A-Z]"),
    re.compile(r"^<!\[CDATA\["),
    re.compile(r"</?(" + _html_6_tags + r")(?:/?>| |$)", re.IGNORECASE),
    re.compile(r"^" + _html_open_tag + r"|" + _html_close_tag + r"[ \t\n\v\f\r]*($)?", re.IGNORECASE)
]

re_html_block_end = [
    re.compile(r"</script>|</pre>|</style>", re.IGNORECASE),
    re.compile(r"-->"),
    re.compile(r"\?>"),
    re.compile(r">"),
    re.compile(r"]]>"),
    re_blank_line,
    re_blank_line
]

re_line_break = re.compile("\u000a|\u000d\u000a?")
re_whitespace = re.compile("[ \t\n\v\f\r]+")
re_not_whitespace = re.compile("[^ \t\n\v\f\r]+")
re_ascii_control = re.compile("[\x00-\x1f]")
#re_paren_set = re.compile(r"^(.*?)(?<!\\)\((.*)(?<!\\)\)(.*)$", re.DOTALL)
re_paren_chars = re.compile(r"(?<!\\)[\(\)]")
